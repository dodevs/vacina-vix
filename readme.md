# BOT ou backend para agendamento automático de vacina

## Requests mapeados da API publica de agendamento

Categorias<br>
https://agendamento.vitoria.es.gov.br/api/categorias

Servicos
https://agendamento.vitoria.es.gov.br/api/categorias/2/servicos, onde 2 é a categoria

Vagas
https://agendamento.vitoria.es.gov.br/api/servicos/1476/unidades/vagas, onde 1476 é o servico

Dias de agendamento
https://agendamento.vitoria.es.gov.br/api/servicos/1386/unidades/13?_=1627931532111, onde 1386 é o servico, 13 a unidade e 1627931532111 um timestamp

Horas disponiveis
https://agendamento.vitoria.es.gov.br/api/servicos/1386/unidades/28/horarios/2021-08-06?_=1627931965932

Agendar<br>
https://agendamento.vitoria.es.gov.br/api/agendamentos

[FORMDATA]<br>

```json
{"data":"2021-08-06","horaDesejada":"15:30","nome":"Josué Armando Costa","documento":"02967791725","tipoDocumento":1,"servico":1388,"unidade":34,"telefone":"27999999999","email":null,"captcha":"03AGdBq25EJSBDNR4czNlfL4-K-o3wUH1TbK82V0qyGTyi0a_OKsoVObdm-6SNbUdFvZfxXCxir0o8tGS_I9f-Q1wDEIrR2CVjsiBWa_P34obo9_g0UZbP_PU9zbOtC9B008Zt8yc-_mMT5RQXr6C5wuFqrgETCcDY6JJDeO5NPvIS0f9xqmCw4ahiO3EpSLXirPVxx75Oe_nt69VSv_SUd59gnsXicgPb-jT6kyoMJutV4uYc1v5LAwjKLDiL7J93d29xRTZ3x2OGPtc8-gZRL0hZui1FpUw7GjzkIDT9B3A08su3mHfw859C7HweOOvauCG5WgOUs9tOT8ucseJZbrEhfBjACGe9MPlg7agQ50Pl1rFIo1JDstaC-SwuzbfUfxyxxFdnkHfPHTZP21jJlcwUmJChvf9f5cixl8HTWc7uj7Qb5Lzm6cYSbSE2WcUR1vyC8n73ckYNO-JDSgMLHcBZZt48v-hmnMbQlL5PZ8v2_idMSXnzg6k","respostas":[]}:
```

[Recaptcha KEY]<br>
6LcwodoaAAAAAKL6uDq4yDzRkzferKf9NUcj0f5a
<br><br>

# Referências Go
https://github.com/imkira/go-observer
https://golangbyexample.com/observer-design-pattern-golang/

https://www.nerd.vision/post/dependency-injection-in-go
https://elliotchance.medium.com/a-new-simpler-way-to-do-dependency-injection-in-go-9e191bef50d5

https://blog.logrocket.com/making-http-requests-in-go/

https://www.sohamkamani.com/golang/2018-06-20-golang-factory-patterns/
http://changelog.ca/log/2015/01/30/golang
package models

type TaskRequest struct {
	ClientKey    string                 `json:"clientKey"`
	Task         map[string]interface{} `json:"task"`
	SoftId       string                 `json:"softId"`
	LanguagePool string                 `json:"languagePool"`
}

func NewTaskRequest(clientKey string, task map[string]interface{}, softId string, languagePool string) TaskRequest {
	return TaskRequest{
		ClientKey:    clientKey,
		Task:         task,
		SoftId:       softId,
		LanguagePool: languagePool,
	}
}

package task

type TaskType string

const (
	ImageToText                    TaskType = "ImageToTextTask"
	RecaptchaV2                    TaskType = "RecaptchaV2Task"
	RecaptchaV2Proxyless           TaskType = "RecaptchaV2TaskProxyless"
	RecaptchaV3Proxyless           TaskType = "RecaptchaV3TaskProxyless"
	RecaptchaV2Enterprise         TaskType = "RecaptchaV2EnterpriseTask"
	RecaptchaV2EnterpriseProxyless TaskType = "RecaptchaV2EnterpriseTaskProxyless"
)

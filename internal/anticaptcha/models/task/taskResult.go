package task

import "time"

type CreateTaskResult struct {
	ErrorId          int    `json:"errorId"`
	ErrorCode        string `json:"errorCode"`
	ErrorDescription string `json:"errorDescription"`
	TaskId           int    `json:"taskId"`
}

type Solution struct {
	Text string `json:"text"`
	Url  string `json:"url"`
}

type TaskResult struct {
	ErrorId    int           `json:"errorId"`
	Status     string        `json:"status"`
	Solution   *Solution     `json:"solution"`
	Cost       string        `json:"cost"`
	Ip         string        `json:"ip"`
	CreateTime time.Duration `json:"createTime"`
	EndTime    time.Duration `json:"endTime"`
	SolveCount int           `json:"solveCount"`
}

func (tr *CreateTaskResult) WaitSolution() TaskResult {
	// Call GetTaskResult after five secconds and returns if "status" is "ready", else wait for more two secconds before try again
	
	return TaskResult{}
}

package task

import (
	"encoding/json"
)

type recaptchaV2TaskProxyless struct {
	Type       TaskType `json:"type"`
	WebSiteURL string   `json:"websiteURL"`
	WebSiteKey string   `json:"websiteKey"`
}

func NewRecaptchaV2TaskProxyless(websiteURL, websiteKey string) *recaptchaV2TaskProxyless {
	return &recaptchaV2TaskProxyless{
		Type:       RecaptchaV2Proxyless,
		WebSiteURL: websiteURL,
		WebSiteKey: websiteKey,
	}
}

func (t *recaptchaV2TaskProxyless) ToRequest() *map[string]interface{} {
	var task map[string]interface{}
	data, err := json.Marshal(t)
	if err != nil {
		panic(err)
	}
	if err = json.Unmarshal(data, &task); err != nil {
		panic(err)
	}
	return &task
}

package anticaptcha

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	requests "github.com/dodevs/vac_vix/internal/anticaptcha/models"
	tasks "github.com/dodevs/vac_vix/internal/anticaptcha/models/task"
)

const (
	BASE_API = "https://api.anti-captcha.com/"
)

type AnticaptchaAPI interface {
	CreateTask(task tasks.Task) *tasks.CreateTaskResult
	GetTaskResult(taskId string) *tasks.TaskResult
}

type anticaptchaAPI struct {
	ClientKey string
}

func NewAnticaptchaAPI(clientKey string) AnticaptchaAPI {
	return &anticaptchaAPI{
		ClientKey: clientKey,
	}
}

func (a *anticaptchaAPI) CreateTask(task tasks.Task) *tasks.CreateTaskResult {
	request := requests.NewTaskRequest(a.ClientKey, *task.ToRequest(), "0", "en")

	_json, err := json.MarshalIndent(request, "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(_json))

	postBody, _ := json.Marshal(request)
	responseBody := bytes.NewBuffer(postBody)

	resp, err := http.Post(BASE_API+"createTask", "application/json", responseBody)

	if err != nil {
		return &tasks.CreateTaskResult{}
	}
	defer resp.Body.Close()

	var response tasks.CreateTaskResult

	err = json.NewDecoder(resp.Body).Decode(&response)

	if err != nil {
		return &tasks.CreateTaskResult{}
	}

	return &response
}

func (a *anticaptchaAPI) GetTaskResult(taskId string) *tasks.TaskResult {

	resp, err := http.Get(BASE_API + "getTaskResult?taskId=" + taskId)

	if err != nil {
		return &tasks.TaskResult{}
	}
	defer resp.Body.Close()

	var response tasks.TaskResult

	err = json.NewDecoder(resp.Body).Decode(&response)

	if err != nil {
		return &tasks.TaskResult{}
	}

	return &response
}

package main

import (
	"encoding/json"
	"fmt"

	"github.com/dodevs/vac_vix/internal/anticaptcha"
	"github.com/dodevs/vac_vix/internal/anticaptcha/models/task"

	"github.com/joho/godotenv"
)

const (
	ANTICAPTCHA_CLIENT_KEY = getEnvVariable("ANTICAPTCHA_CLIENT_KEY")
	AGENDAMENTO_RECAPTCHA_KEY = getEnvVariable("AGENDAMENTO_RECAPTCHA_KEY")
	AGENDAMENTO_SITE_URL      = getEnvVariable("AGENDAMENTO_SITE_URL")
)

func getEnvVariable(variable string) string {
	err := gotodenv.Load(".env")

	return os.Getenv(variable)
}

func main() {
	anticaptchaApi := anticaptcha.NewAnticaptchaAPI(ANTICAPTCHA_CLIENT_KEY)

	recaptchav2task := task.NewRecaptchaV2TaskProxyless(AGENDAMENTO_SITE_URL, AGENDAMENTO_RECAPTCHA_KEY)
	result := anticaptchaApi.CreateTask(recaptchav2task)

	json, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(json))

}
